<?php
/**
 * logAllMail : log all mail with information
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <http://www.sondages.pro>
 * @license AGPL
 * @version 0.0.0
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * The MIT License
 */
class logAllMail extends \ls\pluginmanager\PluginBase
{
    static protected $description = 'Log all mail';
    static protected $name = 'logAllMail';

    public function init()
    {
        $this->subscribe('beforeTokenEmail');
    }

    /**
     * @link https://manual.limesurvey.org/BeforeTokenEmail
     */
    public function beforeTokenEmail()
    {
        /* Quick sytem to log email */
        $aToken=$this->event->get("token");
        if($aToken){
            $sLog='beforeTokenEmail for '.$this->event->get("survey");
            $sLog.=',';
            $sLog.=isset($aToken['tid']) ? $aToken['tid'] : "notset";
            if(Yii::app() instanceof CConsoleApplication) {
                 $sLog.=',console';
            } else {
                $sLog.=','.Yii::app()->session['loginID'];
            }
            $sLog.=',';
            $sLog.=isset($aToken['participant_id']) ? $aToken['participant_id'] : "notset";
            $sLog.=',';
            $sLog.=$this->event->get("type");
            $sLog.=',';
            $sLog.=isset($aToken['email']) ? $aToken['email'] : "notset";
            if(Yii::app() instanceof CConsoleApplication) {
                 $sLog.=',console';
                 $sLog.=',console';
            } else {
                if(Yii::app()->getController()) {
                    $sLog.=','. Yii::app()->getController()->getId();
                    if(Yii::app()->getController()->getAction()) {
                        $sLog.=','. Yii::app()->getController()->getAction()->getId();
                    } else {
                        $sLog.=',notset';
                    }
                } else {
                    $sLog.=',notset';
                    $sLog.=',notset';
                }
            }
            Yii::log($sLog, 'info','application.plugins.logAllMail');
        }
    }
}
